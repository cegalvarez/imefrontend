'use strict';

/**
 * @ngdoc overview
 * @name imeApp
 * @description
 * # imeApp
 *
 * Main module of the application.
 */

var imeApp = angular.module('imeApp', ['ngRoute', 'ngAnimate', 'ngCookies', 'ngResource', 'ngTouch', 'ngSanitize', 'ui.bootstrap', 'imeAppControllers', 'imeAppServices', 'imeAppDirectives']);

var options = {};
var nombre = "";
options.api = {};
options.api.base_url = "http://inversionesmariahelena.com:3000";
var PageSide = 'views/login.html';
var imeAppServices = angular.module('imeAppServices', []);
var imeAppControllers = angular.module('imeAppControllers', []);
var imeAppDirectives = angular.module('imeAppDirectives', []);

  imeApp.config(['$locationProvider', '$routeProvider',
  function($location, $routeProvider) {
    $routeProvider.
        when('/', {
            templateUrl: PageSide,
            controller: 'UserCtrl',
            access: { requiredAuthentication: false }
        }).
        when('/gain', {
            templateUrl: 'views/gain.html',
            controller: 'GainCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/rutes', {
            templateUrl: 'views/routes/index.html',
            controller: 'RutesCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/:id/clients/:badget', {
            templateUrl: 'views/clients/index.html',
            controller: 'ClientsCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/:id/loans', {
            templateUrl: 'views/loans/index.html',
            controller: 'LoansCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/liquidity', {
            templateUrl: 'views/liquidity.html',
            controller: 'LiquidityCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/blacklist', {
            templateUrl: 'views/blacklist.html',
            controller: 'BlacklistCtrl',
            access: { requiredAuthentication: true }
        }).
        when('/main', {
            templateUrl: 'views/main.html',
            controller: 'NameCtrl',
            access: { requiredAuthentication: true }
        }).
        otherwise({
            redirectTo: '/'
        });
}]);
 
imeApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
});

imeApp.run(function($window, $rootScope, $location, AuthenticationService) {
    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication && !AuthenticationService.isLogged && !$window.sessionStorage.token) {
            $location.path("/login");
        }
    });
});

imeAppControllers.controller('UserCtrl', ['$scope', '$location', '$window', 'UserService', 'AuthenticationService',
    function UserCtrl($scope, $location, $window, UserService, AuthenticationService) {
        //Admin User Controller (login, logout)
        $scope.logIn = function logIn(username, password) {
            if (username !== undefined && password !== undefined) {

  
                UserService.signIn(username, password).success(function(data) {
                    
                    $window.sessionStorage.name = data.ruta.name;
                    sessionStorage.name = data.ruta.name;
                    nombre=data.ruta.name;
                    
                    $window.sessionStorage.role = data.ruta.role;
                    sessionStorage.role = JSON.stringify(data.ruta.role);

                    if(data.ruta.role==1){
                    AuthenticationService.isLogged = true;
                    $window.sessionStorage.token = data.cookie;

                    console.log("Si paso el login");
                    $location.path("/main");
                    }else{
                    AuthenticationService.isLogged = true;
                    $window.sessionStorage.token = data.cookie;

                    console.log("Si paso el login");
                    $location.path("/main");
                    }
                   
        

               }).error(function(status, data) {
                      alert("Hubo un error, contraseña o nombre de usuario incorrecto");
                });

            }
        }
 
        $scope.logout = function logout() {
            if (AuthenticationService.isLogged) {
                AuthenticationService.isLogged = false;
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.name;
                delete $window.sessionStorage.role;
                $location.path("/");
            }
        }

        if(AuthenticationService.isLogged==true && $window.sessionStorage.token){
            $location.path("/main");
        }
        
    }
]);

imeAppControllers.controller('NameCtrl', ['$scope',
    function NameCtrl($scope) {
        $scope.nombre = sessionStorage.name;
        $scope.rol = sessionStorage.role;
    }
]);

imeAppServices.factory('AuthenticationService', function() {
    var auth = {
        isLogged: false
    }
 
    return auth;
});

imeAppServices.factory('TokenInterceptor', function ($q, $window, $location, AuthenticationService) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers.Authorization = $window.sessionStorage.token;
            }
            return config;
        },

        requestError: function(rejection) {
            return $q.reject(rejection);
        },

        response: function (response) {
            
            if (response != null && (response.status == 200 || response.status == 201 || response.status == 204) && $window.sessionStorage && !AuthenticationService.isLogged) {
                AuthenticationService.isLogged = true;
            }
            return response || $q.when(response);
        },

        /* Revoke client authentication if 401 is received */
        responseError: function(rejection) {
            if (rejection != null && (rejection.status === 401 || rejection.status === 402 || rejection.status === 400 || rejection.status === 404 || rejection.status === 422 || rejection.status === 429 || rejection.status === 500 || rejection.status === 503) && ($window.sessionStorage.token || AuthenticationService.isLogged)) {
                delete $window.sessionStorage.token;
                AuthenticationService.isLogged = false;
                $location.path("/login");
            }

            return $q.reject(rejection);
        }
    };
});

imeAppServices.factory('UserService', function ($http) {
    return {
        signIn: function(username, password) {
           var json={ruta:{id:username, password:password}};
          {
             return $http.post(options.api.base_url + '/ruta/login', json);
          }
        },

        logOut: function() {
            return $http.get(options.api.base_url + '/logout');
        }
    }
});


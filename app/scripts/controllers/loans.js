'use strict';

/**
 * @ngdoc function
 * @name imeApp.controller:LoansCtrl
 * @description
 * # LoansCtrl
 * Controller of the imeApp
 */

angular.module('imeApp')
  .controller('LoansCtrl', function ($scope,$http,$modal,$routeParams) {
    $scope.rol = sessionStorage.role;
    $scope.url="http://inversionesmariahelena.com:3000/prestamo/";
    console.log($scope.url);
      var req = {
       method: 'GET',
       url: $scope.url+$routeParams.id,
       headers: {
         'Content-Type': "application/json",
         'Authorization': sessionStorage.token
       }
    };
    $http(req).success(function(data){
        console.log("cosa");
        console.log(data);
       $scope.loans =data.Prestamo;
  })

  	
  	
  	$scope.edit = function(data_index, data){
      //console.log(data);
      $scope.loan=data;
  		var modalInstance = $modal.open({
  			templateUrl: 'views/loans/modals/edit.html',
  			controller: 'EditLoanCtrl',
        resolve:{
          thisIndex:function(){return data_index},
          thisLoan: function(){return data}
        }
  		});
      modalInstance.result.then(function(result){
        



        var updateId= $scope.loans[data_index].id;
        
          
          var req = {
           method: 'PUT',
           url: $scope.url+updateId,
           headers: {
             'Content-Type': "application/json",
             'Authorization': sessionStorage.token
           },
           data:{'prestamo':$scope.loans[data_index]}
          };
          $http(req).success(function(data){
            console.log(result);
            $scope.loans[data_index]= result;
          });






        
      });
  	};

	 
  	$scope.delete = function(data_index){
  		var modalInstance = $modal.open({
  			templateUrl: 'views/modals/deleteGeneralModal.html',
  			controller: 'DeleteElementCtrl',
        resolve:{
          thisIndex: function(){return data_index}
        }
  		});

      modalInstance.result.then(function(result){


        var updateId=$scope.loans[data_index].id;

          var req = {
           method: 'DELETE',
           url: $scope.url+updateId,
           headers: {
             'Content-Type': "application/json",
             'Authorization': sessionStorage.token
             }
           };
          $http(req).success(function(data){
            console.log(result);
            $scope.loans.splice(result,1);
          });
        


        
      });
	  };

	 
  	$scope.add = function(){
		  $scope.newLoans={};
		  var modalInstance = $modal.open({
			templateUrl: 'views/loans/modals/CreateLoanModal.html',
			controller: 'AddNewLoansCtrl',
		
  		resolve: {
        newLoans: function() {
            return $scope.newLoans;
        	}
    	}
      });

      modalInstance.result.then(function(selectedItem) {
        $scope.loans.push({
          no: $scope.loans.length + 1,
          nameclient: $scope.newLoans.nameclient,
          cedulaclient: $scope.newLoans.cedulaclient,
          amountloans: $scope.newLoans.amountloans,
          interest: $scope.newLoans.interest,
          loandate: $scope.newLoans.loandate,
          paymentdatelimit: $scope.newLoans.paymentdatelimit,
          paymentdate: $scope.newLoans.paymentdate,
          state: $scope.newLoans.state
		    });
    	});
    };

    $scope.pay = function(data_index){
        console.log(data_index);
        var modalInstance = $modal.open({
        templateUrl: 'views/loans/modals/pay.html',
        controller: 'PayLoanCtrl'
        
      });
      modalInstance.result.then(function(paid){
        var updateId= $scope.loans[data_index].id;
        if(paid){

          $scope.loans[data_index].state= true;

          var req = {
           method: 'PUT',
           url: $scope.url+updateId,
           headers: {
             'Content-Type': "application/json",
             'Authorization': sessionStorage.token
           },
           data:{'prestamo':$scope.loans[data_index]}
          };
          $http(req).success(function(data){
            console.log("esta mierda funcino");
            console.log(data);
          });
        


          
        }
        
      });
    }

  })
	
	.controller('DeleteElementCtrl', function($scope, $modalInstance, thisIndex){
  	$scope.cancel=function(){
  		$modalInstance.dismiss('cancel');
  	};
    $scope.deleteConfirm= function(){
      $modalInstance.close(thisIndex);
    };
  })

  .controller('EditLoanCtrl', function($scope, $modalInstance, thisLoan, thisIndex){
  	$scope.loan=thisLoan;
    $scope.cancel=function(){
  		$modalInstance.dismiss('cancel');
  	};
    $scope.editLoan= function(){
      $modalInstance.close($scope.loan);
    };

  })
  /* Cerrar Modal */
  .controller('AddNewLoansCtrl', function($scope, $modalInstance, newLoans){
  	$scope.newLoans=newLoans;
  	$scope.snewLoans=function(){
  		$modalInstance.close(newLoans);
  	};
  	$scope.cancel=function(){
  		$modalInstance.dismiss('cancel');
  	};
  	
  })

  .controller('PayLoanCtrl', function($scope, $modalInstance){
    $scope.payConfirm=function(){
      $modalInstance.close(true);
    };
    $scope.cancel=function(){
      $modalInstance.dismiss('cancel');
    };
    
  });;


'use strict';

/**
 * @ngdoc function
 * @name imeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the imeApp
*/

angular.module('imeApp')
  .controller('MainCtrl', function ($scope,$http) {
   
  $scope.url='http://inversionesmariahelena.com:3000/ruta/email';
  $scope.formData = {};

	  $scope.enviarEmail = function(){
	  	/*alert('valid form!\n' + JSON.stringify($scope.formData, null, '\t'))*/
	  var req = {
	      method: 'POST',
	      url: $scope.url,
	      data: {
		    'email':{
			'nombre': $scope.formData.name,
			'mensaje': $scope.formData.body
			}
		  },
	    };


	    $http(req).success(function(data){
	        alert("Se envío tu email");
	    }).error(function(){
	        alert("Hubo intentalo de nuevo");
	    });

	   };
});

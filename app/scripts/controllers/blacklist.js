'use strict';

/**
 * @ngdoc function
 * @name imeApp.controller:BlacklistCtrl
 * @description
 * # BlacklistCtrl
 * Controller of the imeApp
 */
angular.module('imeApp')
  .controller('BlacklistCtrl', function ($scope,$http,$modal) {


    console.log(sessionStorage.token);
    var req = {
       method: 'GET',
       url: 'http://inversionesmariahelena.com:3000/cliente/listanegra',
       headers: {
         'Content-Type': "application/json",
         'Authorization': sessionStorage.token
       }
    };


  	$http(req).success(function(data){
      console.log(data);
	    $scope.blacklist =data.cliente;

	})

	$scope.rescue = function(client, data_index){
    	var modalInstance = $modal.open({
			templateUrl: 'views/modals/confirmModal.html',
			controller: 'rescueCtrl',
	      	resolve:{
	       		thisClient: function(){
	          		return client;
	        	},
	        	thisIndex: function(){
	        		return data_index;
	        	}

	      	}
		});
    modalInstance.result.then(function(result){
      //Push to DB-loan

      client.blist= false;
      console.log(client);
      var req = {
         method: 'PUT',
         url: "http://inversionesmariahelena.com:3000/cliente/"+client.id,
         headers: {
           'Content-Type': "application/json",
           'Authorization': sessionStorage.token
         },
         data:{'cliente':client}
        };
        $http(req).success(function(data){
          console.log("holiii");
          $scope.blacklist.splice(result,1);
        });

      
    });
	};

  })
  .controller('rescueCtrl', function($scope, $modalInstance, thisClient, thisIndex){
  	
  	$scope.confirm=function(){
  		$modalInstance.close(thisIndex);
  	};
  	$scope.cancel=function(){
  		$modalInstance.dismiss('cancel');
  	};
  	
  });


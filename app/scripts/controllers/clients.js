'use strict';

/**
 * @ngdoc function
 * @name imeApp.controller:RuteCtrl
 * @description
 * # RuteCtrl
 * Controller of the imeApp
 */
angular.module('imeApp')
  .controller('ClientsCtrl', function ($scope,$http,$modal,$routeParams) {
    $scope.rol = sessionStorage.role;
    console.log(sessionStorage.token);
    $scope.clients=[];
    $scope.max_badget= $routeParams.badget;
    $scope.url="http://inversionesmariahelena.com:3000/cliente/";
    $scope.url2="http://inversionesmariahelena.com:3000/clienteRuta/";
    $scope.myNewLoan= {};
    var req = {
     method: 'GET',
     url: $scope.url2+$routeParams.id,
     headers: {
       'Content-Type': "application/json",
       'Authorization': sessionStorage.token
     }
  };
    $http(req).success(function(data){
      $scope.clients =data.Ruta.clients;
      $scope.name= data.name;
  })
  //Loanfor CLient
  $scope.newLoan = function(index_data){
    $scope.myNewLoan= {};
    $scope.max_badget= $routeParams.badget;
    console.log($scope.max_badget);
    console.log("hidfsfds");
    var modalInstance = $modal.open({
      templateUrl: 'views/loans/modals/new.html',
      controller: 'CreateLoanCtrl',
      resolve:{
        thisLoan: function(){
          return $scope.myNewLoan;
        },
        thisMax: function(){
          return $scope.max_badget
        }
      }
    });
    modalInstance.result.then(function(result){
      var req = {
         method: 'POST',
         url: "http://inversionesmariahelena.com:3000/prestamo/",
         headers: {
           'Content-Type': "application/json",
           'Authorization': sessionStorage.token
         },
         data:{
          'cliente':{
            'id': $scope.clients[index_data].id
          },
            'prestamo':result
          
        }
      };
        $http(req).success(function(data){
          console.log(data);
          
      });
    });
  };

  $scope.editClient = function(index_data, data){
    $scope.client= data;
    var modalInstance = $modal.open({
      templateUrl: 'views/clients/modals/editClient.html',
      controller: 'EditClientCtrl',
      resolve:{
        thisIndex:function(){
          return index_data;
        },
        thisClient: function(){
          return $scope.client;
        }
      }
    });
     modalInstance.result.then(function(result) {
        console.log(result);

        var req = {
         method: 'PUT',
         url: $scope.url+result.id,
         headers: {
           'Content-Type': "application/json",
           'Authorization': sessionStorage.token
         },
         data:{'cliente':result}
        };
        $http(req).success(function(data){
          $scope.clients[index_data]=result;
        });
        
     });

  };

  $scope.deleteClient = function(index_data){
    var modalInstance = $modal.open({
      templateUrl: 'views/modals/deleteGeneralModal.html',
      controller: 'DeleteClientCtrl',
      resolve: {
        thisIndexData: function(){return index_data}
      }
    });

    modalInstance.result.then(function(result){
      console.log(result);
      var deleteId= $scope.clients[result].id;
      var req = {
       method: 'DELETE',
       url: $scope.url+deleteId,
       headers: {
         'Content-Type': "application/json",
         'Authorization': sessionStorage.token
       },
       data:{'cliente':result}
      };
      $http(req).success(function(data){
       $scope.clients.splice(result,1);
      });
        

    });
  };

  $scope.addToBlacklist = function(index_data, flag){
    var modalInstance = $modal.open({
      templateUrl: 'views/modals/confirmModal.html',
      controller: 'addToBlacklisttCtrl',
      resolve: {
        thisIndexData: function(){return index_data}
      }
  });

    modalInstance.result.then(function(result){

      $scope.clients[result].blist= true;
      
      var req = {
         method: 'PUT',
         url: $scope.url+$scope.clients[result].id,
         headers: {
           'Content-Type': "application/json",
           'Authorization': sessionStorage.token
         },
         data:{'cliente':$scope.clients[result]}
        };
        $http(req).success(function(data){
          $scope.clients[result].blist= true;
          console.log(data);
        });

    });
  };

  $scope.addClient = function(){
    $scope.newClient={};
    var modalInstance = $modal.open({
      templateUrl: 'views/clients/modals/addClient.html',
      controller: 'AddNewClientCtrl',
    
      resolve: 
      {
        newClient: function() {
          return $scope.newClient;
        }
      }
    });

    modalInstance.result.then(function(result) {

      console.log(result);
      console.log($routeParams.id);
      var req = {
         method: 'POST',
         url: $scope.url,
         headers: {
           'Content-Type': "application/json",
           'Authorization': sessionStorage.token
         },
         data:{'cliente':result}
      };
        $http(req).success(function(data){
          console.log(data);
          $scope.clients.push({
            nameclient: $scope.newClient.nameclient,
            id: $scope.newClient.id,
            cellphone: $scope.newClient.cellphone,
            landline: $scope.newClient.landline,
            address: $scope.newClient.address
          });
       });


      
    });
  };
})
  
  //______________________________________________________
  //______________________________________________________
  
  //    CONTROLLERS
  //______________________________________________________
  //______________________________________________________
  
  //Controller for create Loan
.controller('CreateLoanCtrl', function($scope, $modalInstance, thisLoan,thisMax){
    $scope.max_badget= thisMax;
    $scope.thisLoan=thisLoan;
    $scope.new=function(){
      console.log("hola loka");
      console.log($scope.myNewLoan);
      $modalInstance.close($scope.myNewLoan);
    };
    $scope.cancel=function(){
      $modalInstance.dismiss('cancel');
    };
    
  })
//Controller for edit Client
.controller('EditClientCtrl', function($scope, $modalInstance, thisIndex, thisClient){
  $scope.client= thisClient;
  $scope.cancel=function(){
    $modalInstance.dismiss('cancel');
  };
  $scope.edit=function(){
    console.log($scope.client);
    $modalInstance.close($scope.client);
  };
})
//Controller for delete Client
.controller('DeleteClientCtrl', function($scope, $modalInstance,thisIndexData){
  $scope.cancel=function(){
    $modalInstance.dismiss('cancel');
  };
  $scope.deleteConfirm=function(){
    $modalInstance.close(thisIndexData);
  };
})

//Controller for add new Client
.controller('AddNewClientCtrl', function($scope, $modalInstance, newClient){
  $scope.newClient=newClient;
  $scope.saveNewClient=function(){
    $modalInstance.close(newClient);
  };
  $scope.cancel=function(){
    $modalInstance.dismiss('cancel');
  };
})
.controller('addToBlacklisttCtrl', function($scope, $modalInstance,thisIndexData){
  $scope.cancel=function(){
    $modalInstance.dismiss('cancel');
  };
  $scope.confirm=function(){
    $modalInstance.close(thisIndexData);
  }; 
})
;

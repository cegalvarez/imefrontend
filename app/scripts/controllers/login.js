'use strict';

/**
 * @ngdoc function
 * @name imeApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the imeApp
 */
angular.module('imeApp')
  .controller('LoginCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });

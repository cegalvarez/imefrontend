'use strict';

angular.module('imeApp')
  .controller('RutesCtrl', function ($scope,$http,$modal,$window) {

    $scope.url='http://inversionesmariahelena.com:3000/ruta/';

  	console.log(sessionStorage.token);
    var req = {
       method: 'GET',
       url: 'http://inversionesmariahelena.com:3000/ruta',
       headers: {
         'Content-Type': "application/json",
         'Authorization': sessionStorage.token
       }
    };

    $http(req).success(function(data){
        $scope.rutes =data.ruta
        console.log(data);
        console.log(data[1].id);
  	})
  $scope.alerts=function(flag, message){

  }

  $scope.rol = $window.sessionStorage.role;
  	/* Modal delete scope  */
    /*receive index of item, send this to controller*/
  $scope.showDeleteModal = function(index){
  		var modalInstance = $modal.open({
  			templateUrl: 'views/modals/deleteGeneralModal.html',
  			controller: 'DeleteElementCtrl',
        resolve:{
          thisIndex:function(){
            return index;
          }
        }
  		});
      //Modal Action
      modalInstance.result.then(function(result){
       console.log(result);
          var deleteId= $scope.rutes[result].id;
          console.log(deleteId);
          console.log(sessionStorage.token);
          var req = {
             method: 'DELETE',
             url: $scope.url+deleteId,
             headers: {
               'Content-Type': "application/json",
               'Authorization': sessionStorage.token
             }
          };
          $http(req).success(function(data){
              $scope.rutes.splice(result,1);
          });

      });
  	};

	   /* Modal edit scope  */
  	$scope.edit = function(data_route, data_index){

      $scope.route=data_route;
      var modalInstance = $modal.open({
  			templateUrl: 'views/routes/modals/edit.html',
  			controller: 'EditRuteCtrl',
        resolve:{
          thisRoute:function(){
            return data_route;
          }
        }
  		});

      modalInstance.result.then(function(result){
        console.log(result);
        if(result){ 
          var updateId= $scope.rutes[data_index].id;
          console.log(updateId);
          var req = {
             method: 'PUT',
             url: $scope.url+updateId,
             headers: {
               'Content-Type': "application/json",
               'Authorization': sessionStorage.token
             },
             data: {'ruta': result},
          };
          $http(req).success(function(data){
              console.log(data);
          }).error(function(){
            console.log("pailander");
          });
        }


      });
  	};
   /* Modal add_route scope  */
  	$scope.new = function(){
  		$scope.newRute={};
  		var modalInstance = $modal.open({
  			templateUrl: 'views/routes/modals/new.html',
  			controller: 'AddNewRuteCtrl',
    		resolve:
        {
          newRute: function() {
            return $scope.newRute;
          }
    	  }
      });

      modalInstance.result.then(function(result) {
        console.log(result);
        if(result){ 
          var req = {
             method: 'POST',
             url: $scope.url,
             headers: {
               'Content-Type': "application/json",
               'Authorization': sessionStorage.token
             },
             data: {'ruta': result},
          };
          $http(req).success(function(data){
              console.log(data);
              $scope.rutes.push({
                name: data.ruta.name,
                badget: data.ruta.badget,
                password: data.ruta.password,
                cellphone: data.ruta.landline,
                landinline: data.ruta.tellphone,
                city: data.ruta.city,
                clients: data.ruta.clients
              });
          }).error(function(){
            console.log("pailander");
          });
        }
        
  	  });
    };
  })

  .controller('DeleteElementCtrl', function($scope, $modalInstance, thisIndex){
  	console.log(thisIndex);
    $scope.deleteConfirm= function(){
      $modalInstance.close(thisIndex);
    };
    $scope.cancel=function(){
  		//---------------------------------------------------
        // AGERGAR FUNCIONALIDAD,
      //---------------------------------------------------
      $modalInstance.dismiss('cancel');
  	};
  })

  .controller('EditRuteCtrl', function($scope, $modalInstance,thisRoute){
  	//---------------------------------------------------
      // AGERGAR FUNCIONALIDAD,
    //---------------------------------------------------
    console.log("//---------------------------------------------------");
    $scope.route=thisRoute;
    console.log( $scope.route);
    $scope.cancel=function(){
  		$modalInstance.dismiss('cancel');
  	};
    $scope.editRoute=function(){
      console.log($scope.route);
      $modalInstance.close($scope.route);
    }
    
  })

  /* Cerrar Modal */
  .controller('AddNewRuteCtrl', function($scope, $modalInstance, newRute){
  	$scope.newRute=newRute;
  	$scope.snewRute=function(){
      //---------------------------------------------------
      // AQUI HAY QUE COLOCAR UNA ALERTA GENERAL PARA ESTO,
      // preferiblemente con un codicional
      //---------------------------------------------------
  		$modalInstance.close(newRute);
  	};
  	$scope.cancel=function(){
  		$modalInstance.dismiss('cancel');
  	};
  	
  });

  


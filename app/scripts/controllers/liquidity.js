'use strict';

/**
 * @ngdoc function
 * @name imeApp.controller:LiquidityCtrl
 * @description
 * # LiquidityCtrl
 * Controller of the imeApp
 */
'use strict';

angular.module('imeApp')
  .controller('LiquidityCtrl', function ($scope,$http,$modal) {
  	console.log(sessionStorage.token);
  	var req = {
	 method: 'GET',
	 url: 'http://inversionesmariahelena.com:3000/prestamo/liquidez',
	 headers: {
	   'Content-Type': "application/json",
	   'Authorization': sessionStorage.token
	 }
	};
  	$http(req).success(function(data){
  		console.log("funciono");

  		console.log(data);

    	$scope.liquidity =data;

	})
  });

  


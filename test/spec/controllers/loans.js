'use strict';

describe('Controller: LoansCtrl', function () {

  // load the controller's module
  beforeEach(module('imeApp'));

  var LoansCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LoansCtrl = $controller('LoansCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

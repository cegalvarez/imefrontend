'use strict';

describe('Controller: RutesCtrl', function () {

  // load the controller's module
  beforeEach(module('imeApp'));

  var RutesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RutesCtrl = $controller('RutesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

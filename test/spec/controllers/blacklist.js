'use strict';

describe('Controller: BlacklistCtrl', function () {

  // load the controller's module
  beforeEach(module('imeApp'));

  var BlacklistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BlacklistCtrl = $controller('BlacklistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

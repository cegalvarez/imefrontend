'use strict';

describe('Controller: RuteCtrl', function () {

  // load the controller's module
  beforeEach(module('imeApp'));

  var RuteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RuteCtrl = $controller('RuteCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});

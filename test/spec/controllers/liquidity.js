'use strict';

describe('Controller: LiquidityCtrl', function () {

  // load the controller's module
  beforeEach(module('imeApp'));

  var LiquidityCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    LiquidityCtrl = $controller('LiquidityCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
